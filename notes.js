// [SECTION] bcrypt
	// npm i bcrypt / npm install bcrypt

	// In our application, we will be using this package to demonstrate how to encrypt data when a user register to our website.

	// the bcrypt package is one of the many packages that we can use to encrypt information but it is not commonly recommended because of how simple the algo is.

	// There are other more advance encryption packages that can be used.

	// Syntax for hashing password
		// Syntax:
			// bcrypt.hashSync(password, saltRounds)
		// saltRounds is the value provided as the number of "salt" that the bcrypt algorithm will run in order to encrypt the password.

// [SECTION] JWT - jsonwebtoken package

	// JSON web tokens is an industry standard for sending information between our application in a secure manner
	// the jsonwebtoken package will allow us to gain access to methods that will help us create JSON web token
	// JSON web token or jwt is a way of securely passing the server to the frontend or other parts of the server
	// Information is kept secure through the use of the secret code
	// Only the system will know the secret code that can decode the encrypted information