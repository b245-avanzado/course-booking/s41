const express = require("express");
const router = express.Router();
const auth = require("../auth.js");

const userController = require("../Controllers/userController.js");


// [ROUTES]

// this route is responsible for the registration of the user
router.post("/register", userController.userRegistration);

// this route is for the user authentication
router.post("/login", userController.userAuthentication);

// this route is for retrieving the details of a user
router.get("/details", auth.verify, userController.getProfile);


// route for user enrollment
router.post("/enroll/:courseId", auth.verify, userController.enrollCourse);






module.exports = router;