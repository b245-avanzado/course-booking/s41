const mongoose = require("mongoose");
const User = require("../Models/usersSchema.js");
const Course = require("../Models/coursesSchema.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");



// Controllers


// This controller will create or register a user on our database
module.exports.userRegistration = (request, response) => {

	const input = request.body;

	User.findOne({email:input.email})
	.then(result => {
		if (result !== null) {
			return response.send("The email is already taken!")
		} else {
			let newUser = new User({
				firstName: input.firstName,
				lastName: input.lastName,
				email: input.email,
				password: bcrypt.hashSync(input.password, 10),
				mobileNo: input.mobileNo
			})

			// save to database
			newUser.save()
			.then(save => {
				return response.send("You are now registered in our website!");
			})
			.catch(error => {
				return response.send(error);
			})
		}
	})
	.catch(error => {
		return response.send(error)
	})
}

// User Authentication
module.exports.userAuthentication = (request, response) => {
	let input = request.body;

	// Possible scenarios in logging in
		// 1. email is not yet registered.
		// 2. email is registered but the password is wrong

	User.findOne({email: input.email})
	.then(result => {
		if (result === null) {
			return response.send("Email is not yet registered. Register first before logging in!")
		} else {
			// we have to verify if the password is correct
			// the "compareSync" method is used to compare a non encrypted password to the encrypted password.
			// it returns boolean value, if match true value will return false otherwise.
			const isPasswordCorrect = bcrypt.compareSync(input.password, result.password);

			if (isPasswordCorrect) {
				return response.send({auth: auth.createAccessToken(result)});
			} else {
				return response.send("Password is incorrect!");
			}
		}
	})
	.catch(error => {
		return response.send(error);
	})
}

// User Details
module.exports.getProfile = (request, response) => {
	// let input = request.body;
	const userData = auth.decode(request.headers.authorization);

	console.log(userData);

	User.findById(userData._id)
	.then(result => {
		result.password = "";

		return response.send(result);
	})
	.catch(error => {
		return response.send(error);
	})

	// .then(result => {
	// 	if (result == null) {
	// 		return response.send("Please enter a valid Id!")
	// 	} else {
	// 		let userDetails = {
	// 			firstName: result.firstName,
	// 			lastName: result.lastName,
	// 			email: result.email,
	// 			password: "",
	// 			mobileNo: result.mobileNo
	// 		};

	// 		return response.send(userDetails);
	// 	}
	// })
	// .catch(error => {
	// 	return response.send(error);
	// })
}

// Controller for user enrollment:
	// 1. We can get the id of the user by decoding the jwt
	// 2. We can get the courseId by using the request params

module.exports.enrollCourse = (request, response) => {
	// First we have to get the userId and the courseId

	// decode the token to extract/unpack the payload
	const userData = auth.decode(request.headers.authorization);

	// get the courseId by targetting the params in the url
	const courseId = request.params.courseId;

	// 2 things that we need to do in this controller
		// First, to push the courseId in the enrollments property of the user
		// Second, to push the userId in the enrollees property of the course

	User.findById(userData._id)
	.then(user => {
		if (user === null) {
			return response.send("Please enter a registered token");
		} else {
			if (user.isAdmin) {
				return response.send("You can not enroll in a course.")
			} else {

				Course.findById(courseId)
				.then(course => {
					if (course == null) {
						return response.send("Invalid course Id. Please enter a valid course Id.")
					} else if (!course.isActive) {
						return response.send("Sorry, this course is not available right now.")
					} else {
						course.enrollees.push({userId: userData._id});
						user.enrollments.push({courseId: courseId});

						user.save()

						course.save()

						return response.send(`${user.firstName}, you are now enrolled in ${course.name}!`)
					}
				})
			}
		}
	})
	.catch(error => response.send(error))

}